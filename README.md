Scripts to manage blocked IPs.   

# Information
* Server: c1s-platform2
* Required Packages:
	* dos2unix
	* mail
	* mount.cifs
* Directories:
	* /opt/admin/script: working dir
	* /opt/admin/script/#old: backup dir
	* /opt/admin/script/remove_blocked_IP: remove IPs and blacklist script
	* /opt/admin/script/remove_blocked_IP_diff: temp dir
	* /opt/admin/script/remove_blocked_IP/log: log dir
* Files (working dir as root):
	* IPlist.lst: contains IPs 're being blocked by apache
	* IPblacklist.lst: contains IPs will be blocked forever, and is part of IPlist.lst
	* unblocked.dat: contains data of IPs and times they 're removed from IPlists.lst
	* remove_blocked_IP/remove_blocked_IP.sh: script run monthly to remove old IP from IPlist
	* remove_blocked_IP/blacklist.sh: script use list of IP unblocked each month to update unblocked.dat and add IPs to IPblacklist.lst, called by remove_blocked_IP.sh
	* remove_blocked_IP/unblacklist.sh: script is used to remove IP from blacklist by delete the IP from IPlist.lst, IPblacklist.lst and comment out that IP information at unblocked.dat.
	* remove_blocked_IP/variables.sh: contains environment variables.

# Guide
1. Remove blocked IP:
This script should be put in crontab to run monthly.   
In case you still want to do it manually, run the following command:   
```
[root@c1s-platform2 ~]# cd /opt/admin/script/remove_blocked_IP
[root@c1s-platform2 remove_blocked_IP]# ./remove_blocked_IP.sh
```
1. Remove IP from blacklist:
Run script unblacklist.sh with parameters are IPs needed to remove from blacklist.  
Keep in mind that those IPs will be removed from IPlist.lst as well.   
```
[root@c1s-platform2 remove_blocked_IP]# ./unblacklist.sh 40.77.167.9 157.55.39.223
```
1. View list of IP that being removed from IPlist 3 times or higher:   
```
[root@c1s-platform2 script]# awk '$1 !~ "#" && $NF>2' unblocked.dat
```
