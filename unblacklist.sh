#!/bin/bash

set -u

WORKING_DIR=$(dirname $0)
source ${WORKING_DIR}/variables.sh
LOG=${LOG_DIR}/remove_blacklist_$(date +%y%m%d%H%M).log


if [[ ! -f $BLACKLIST ]]; then
	echo "[ERROR] blacklist not found"
	exit 1
fi
if [[ ! -f $UNBLOCKED_DB ]]; then
	echo "[ERROR] unblocked db not found"
	exit 1
fi

if [[ ! "$1" ]]; then
	echo "Usage: $0 IP1 [IP2] ...."
	exit 1
fi

# function check if IP is in IPLIST or not
# return 0 if it is, 1 if not
function check_IP() {
	grep -q "^$1$" $IPLIST
	if [[ $? -eq 0 ]]; then
		echo -e "\tThere 's $1 at $IPLIST"
	else
		echo -e "\t${IP} 's not in $IPLIST"
		return 1
	fi
}

# Check http status code of IP at https://www.chip1stop.com/
# return 0 if STATUS is 200
function check_block_status() {
	STATUS=$(curl -H "True-Client-IP: $1" -I https://www.chip1stop.com/ | awk '/^HTTP/ {print $2}')
	case $STATUS in
	200)
		echo "[INFO] $IP: OK"
	;;	
	403)
		echo "[INFO] $IP: Forbidden"
		return 1
	;;
	*)
		echo "[INFO] $IP unknown status $STATUS"
		return 2
	;;
	esac
}

# Delete IP at BLACKLIST
# return code: 0 - IP exists at blacklist
#	       1 - IP not in blacklist but exists at IPlist
#	       2 - IP not exists at both blacklist and IPlist
function remove_blacklist() {
	grep -q "^$1$" $BLACKLIST
	if [[ $? -ne 0 ]]; then
		echo "[INFO] there 's no $1 in blacklist"
		check_IP $1 && return 1 || return 2
	else
		sed "/^$1$/ d" -i $BLACKLIST && \
			echo "[INFO] Removed $1 from blacklist"
	fi
}

# Comment IP at unblocked_db
function comment_db() {
	sed "/^$1[[:blank:]]/ s/^/#/" -i $UNBLOCKED_DB && \
		echo "[INFO] Comment $1 in $UNBLOCKED_DB"
}

# delete IP at IPLIST
function remove_IPlist() {
	sed "/^$1$/ d" -i $IPLIST && \
		echo "[INFO] Removed $1 from $IPLIST"	
}

# run script httpd-rewrite_cron.sh to apply config 
function apply_config() {
# comment out crontab job
	sed '/httpd-rewrite_cron.sh/ s/^/#/' -i /var/spool/cron/root && { echo "[INFO] Disable cron httpd-rewrite script"
		grep 'httpd-rewrite_cron.sh' /var/spool/cron/root
		trap "sed '/httpd-rewrite_cron.sh/ s/^#//' -i /var/spool/cron/root; grep 'httpd-rewrite_cron.sh' /var/spool/cron/root" EXIT
	}
	/opt/admin/script/httpd-rewrite_cron.sh
}

for IP in $@
do
	remove_blacklist $IP
	case $? in
	0) 
		comment_db $IP 
		remove_IPlist $IP
		apply_config
		check_block_status $IP
	;;
	1)
		remove_IPlist $IP
		apply_config
		check_block_status $IP
	;;
	2)
		check_block_status $IP
	;;
	esac
done | tee -a $LOG
