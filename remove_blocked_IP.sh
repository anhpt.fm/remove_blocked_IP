#!/bin/bash

# This script is used to remove blocked IP from block list
# and apply to web servers

WORKING_DIR=$(dirname $0)
source ${WORKING_DIR}/variables.sh
LOG=${LOG_DIR}/remove_blocked_IP-$(date +%y%m%d%H%M).log
DIFF_DIR=${SCRIPT_DIR}/remove_blocked_IP_diff
DIFF_FILE=${DIFF_DIR}/diff_$(date +%y%m%d%H%M)
TIMESTAMP=$(date +%y%m)
# remove IPs at MONTH (months) ago
MONTH=2
OLD_TIMESTAMP=$(date -d "$MONTH months ago" +%y%m)
HTTP_REWRITE_LOG=${SCRIPT_DIR}/httpd-rewrite_cron_${OLD_TIMESTAMP}.log
# IP addresses found in the log file
IPS=$(grep '^[1-9][0-9]\{0,2\}\.\([0-9]\{1,3\}\.\)\{2\}[1-9][0-9]\{0,2\}$' $HTTP_REWRITE_LOG)
BLACKLIST_SCRIPT=${WORKING_DIR}/blacklist.sh
MOUNT_POINT=/mnt/user_web_IP_block
REMOTE_FOLDER='//filesv/ws-support/100_システム/120_システム機能/124_ユーザーWeb_IPブロック/IPブロック定期削除履歴'
F_USER=ws-ope@chip1stop.com
F_PASS=ws-ope
OLD_TOTAL_LENGTH=$(wc -l $IPLIST | awk '{print $1}')
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
MAILTO="lv-sysope@chip1stop.com
aschen@chip1stop.com
saito@chip1stop.com
tnakamura@chip1stop.com
"

[ ! -d $LOG_DIR ]&& mkdir -p $LOG_DIR

function email {
NEW_BLACKLIST_LENGTH=$(wc -l $BLACKLIST | awk '{print $1}')
NEW_TOTAL_LENGTH=$(wc -l $IPLIST | awk '{print $1}')
SEVERITY=$(grep -o 'ERROR\|WARN\|INFO' $LOG)
BLACKLIST_IPS=$(awk '/\[INFO\] append/ {print "\t"$3}' $LOG)
case $SEVERITY in
*ERROR*)
  MAIL_TAG='ERROR'
;;
*WARN*)
  MAIL_TAG='WARN'
;;
INFO*)
  MAIL_TAG='INFO'
;;
esac


echo "
MARKED IP:   ${FIP-N/A}
MARKED Time: ${ANCHOR_TIME-N/A}

BEFORE:
 - Black list length: $BLACKLIST_LENGTH
 - IPlist length:     $IPLIST_LENGTH
 - Total:	      $OLD_TOTAL_LENGTH

AFTER:
 - Black list length:     $NEW_BLACKLIST_LENGTH
 - Iplist length:         $NEW_LIST_LINE
 - Total:	          $NEW_TOTAL_LENGTH
 - Unblocked list length: $UNBLOCKED_LIST_LENGTH

Newly added blacklist IPs:
${BLACKLIST_IPS-N/A}
" | mail -s "[Remove block IP] [$MAIL_TAG] $(date '+%F %T')" -r "$(whoami)@$(hostname -A)" -a $LOG $MAILTO
}

{
# dos2unix is needed by blacklist.sh script
which dos2unix >/dev/null || exit 1


# function running after the script is done. Will be called by trap.
# The function is to uncommented crontab job
function finish {
	sed '/httpd-rewrite_cron.sh/ s/^#//' -i /var/spool/cron/root && {
		echo "[INFO] enable cron http-rewrite script"
		grep 'httpd-rewrite_cron.sh' /var/spool/cron/root
	}
}

# comment out crontab job
sed '/httpd-rewrite_cron.sh/ s/^/#/' -i /var/spool/cron/root && { echo "[INFO] Disable cron httpd-rewrite script"; grep 'httpd-rewrite_cron.sh' /var/spool/cron/root; }
# run finish function upon EXIT
trap "finish>>$LOG;email" EXIT
# check the blacklist
[[ ! -f ${BLACKLIST_SCRIPT} ]]&&{ echo "[ERROR] Missing blacklist script"; exit 1; } 
# backup current blocked IP list
cp -p ${IPLIST} ${SCRIPT_DIR}/\#old/IPlist.lst.`date +%y%m%d_%H%M%S`

# create DIFF_DIR if not exists
[[ ! -f ${DIFF_DIR} ]]&&{ mkdir -p ${DIFF_DIR}; }

# find FIP which is choosen point to extract the new IPlist.
# because there 're cases when IP could be in both IPlist and blacklist
# (the 2 months after newly apply blacklist), further actions like remove
# IP linenum of blacklist in IPlist is needed
for IP in $IPS
do
# line no of IP at blacklist
	BLACK_LINE=$(grep -n "^$IP$" $BLACKLIST | awk -F':' '{print $1}')
# line no of IP at IPlist
	LIST_LINE=$(grep -n "^$IP$" $IPLIST | awk -F':' '{print $1}' | grep -v "^$BLACK_LINE$")
	if [[ "$LIST_LINE" ]]; then
		FIP=$IP
		ANCHOR_TIME=$(grep -m1 -A1 "^$IP$" $HTTP_REWRITE_LOG | awk 'NR==2 {print $1,$2}')
		echo "[INFO] Found marked IP $IP at line $LIST_LINE from IPlist"
		echo "[INFO] Found marked time $ANCHOR_TIME of $IP from $HTTP_REWRITE_LOG"
		break
	fi
done

if [[ ! "$FIP" ]]; then
	echo "[WARN] Not found any IP to remove."
	exit 0
fi

# Extract new IP list
sed -n "$LIST_LINE,$ p" ${IPLIST} > ip_${TIMESTAMP}
NEW_LIST_LINE=$(wc -l ip_${TIMESTAMP} | awk '{print $1}')
[ $NEW_LIST_LINE -eq 0 ]&&{ echo "[ERROR] new IP list temp ip_${TIMESTAMP} is empty. $FIP may not exists in IPlist.lst."; exit 1; }

# I need go through steps: remove blacklist from IPlist and insert it back later
# because I don't want to change the current scripts and workflow
# while implementing the blacklist feature
if [[ -s $BLACKLIST ]]; then
        BLACKLIST_MD5=$(md5sum $BLACKLIST | awk '{print $1}')
        BLACKLIST_LENGTH=$(wc -l $BLACKLIST | awk '{print $1}')
        BLACK_IPLIST_MD5=$(head -n $BLACKLIST_LENGTH $IPLIST | md5sum | awk '{print $1}')
        if [[ "${BLACKLIST_MD5}" == "${BLACK_IPLIST_MD5}" ]]; then
# remove blacklist from IPlist before compare old and new IPlist to create DIFF_FILE
                sed "1,${BLACKLIST_LENGTH} d" $IPLIST | diff - ip_${TIMESTAMP} | sed 's/< //;1d;/^$/ d' > ${DIFF_FILE}
        else
                echo "[ERROR] Blacklist 's MD5SUM is wrong. Check $BLACKLIST and $IPLIST. Abort ..."
                exit 1
        fi
else
        diff $IPLIST ip_${TIMESTAMP} | sed 's/< //;1d;/^$/ d' > ${DIFF_FILE}
fi

# Get information for email
IPLIST_LENGTH=$(($(wc -l $IPLIST | awk '{print $1}') - ${BLACKLIST_LENGTH-0}))
UNBLOCKED_LIST_LENGTH=$(wc -l ${DIFF_FILE} | awk '{print $1}')

# Write result in filesv
[[ ! -d $MOUNT_POINT ]]&&{ mkdir $MOUNT_POINT; }
mount.cifs $REMOTE_FOLDER $MOUNT_POINT -o user=${F_USER}%${F_PASS} && {
	RESULT=$(date +%Y%m%d).txt
	cp -p --backup ${DIFF_FILE} ${MOUNT_POINT}/${RESULT} && echo "[INFO] Created list of unblocked IP (${UNBLOCKED_LIST_LENGTH}) at ${REMOTE_FOLDER}/${RESULT}"
	umount $MOUNT_POINT
} || { echo "[WARN] mount $REMOTE_FOLDER failed, result is not written on filesv. Unblocked list stay at ${DIFF_FILE}"; }

# Update blacklist from DIFF_FILE
${BLACKLIST_SCRIPT} ${DIFF_FILE}

# overwrite extracted list with blacklist and IP list
#mv -vf ip_${TIMESTAMP} ${IPLIST} | tee -a $LOG
cat $BLACKLIST ip_${TIMESTAMP} > $IPLIST
rm -vf ip_${TIMESTAMP}
# Apply new setting to web servers
${SCRIPT_DIR}/httpd-rewrite_DoS.conf_web.sh 2>&1 >> ${SCRIPT_DIR}/httpd-rewrite_cron_${TIMESTAMP}.log || echo "[ERROR] Apply setting for new IPlist failed."
} > $LOG 2>&1

