#!/bin/bash

WORKING_DIR=$(dirname $0)
source ${WORKING_DIR}/variables.sh
LIMIT=3
## UNBLOCKED_LIST is the file contains IPs were removed from blocked list
## Because some of UNBLOCKED_LIST files is in windows format, we need to convert it to unix format
UNBLOCKED_LIST=/tmp/blacklist.tmp-$$

if [[ ! -f $1 ]]; then 
	echo "[ERROR] unblocked list $1 not found"
	exit 1
fi

[[ ! -f $BLACKLIST ]]&&{ touch $BLACKLIST; }
[[ ! -f $UNBLOCKED_DB ]]&&{ echo ' ' > $UNBLOCKED_DB; }
## checksum is necessary to avoid a list is counted twice
MD5SUM=$(md5sum $1 | awk '{print $1}')
cp -p $1 $UNBLOCKED_LIST
dos2unix $UNBLOCKED_LIST
trap 'rm -f $UNBLOCKED_LIST' EXIT
grep $MD5SUM $UNBLOCKED_DB >/dev/null 2>&1
if [ $? -eq 0 ]; then
	echo "[WARN] $1 ($MD5SUM) 's already added to $UNBLOCKED_DB. Abort ...."
	exit 0
else
	sed "1i # $(basename $UNBLOCKED_LIST)\t${MD5SUM}" -i $UNBLOCKED_DB
fi

grep -v '^[[:blank:]]*$\|^[[:blank:]]*#' $UNBLOCKED_LIST | while read IP
do
## check if IP is comment on UNBLOCKED_DB
	grep -q "#$IP[[:blank:]]" $UNBLOCKED_DB
	if [ $? -eq 0 ];then
		echo "[WARN] $IP is commented on $UNBLOCKED_DB"
		continue
	fi
## check if IP is listed on UNBLOCKED_DB
	grep -q "^$IP[[:blank:]]" $UNBLOCKED_DB
	if [ $? -eq 0 ]; then
## NUM is the number of times an IP was removed from IPlist (unblock)
		NUM=$(awk -v ip=$IP '$1 == ip {print $2}' $UNBLOCKED_DB)
		U_NUM=$(($NUM + 1))
## Increase NUM by one in DB
		sed "/^${IP}[[:blank:]]/ s/${NUM}\$/${U_NUM}/" -i $UNBLOCKED_DB
		if [[ $U_NUM -ge $LIMIT ]]; then
			grep -q "^$IP$" $BLACKLIST && echo "[INFO] $IP already exists at $BLACKLIST.Ignore..." || {
				echo "${IP}" >> $BLACKLIST
	#			sed "/^${IP}[[:blank:]]/ d" -i $UNBLOCKED_DB
				echo "[INFO] append $IP to blacklist"
			}
		fi
	else
		echo -e "${IP}\t1" >>  $UNBLOCKED_DB
	fi
done
